const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://olesia:214VWlh2sqFoWjjK@cluster0.jf9rdzg.mongodb.net/?retryWrites=true&w=majority');

const { authRouter } = require('./authRouter');
const { notesRouter } = require('./notesRouter');
const { usersRouter } = require('./usersRouter');

const PORT = 8080;

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', notesRouter);
app.use('/api/users', usersRouter);

function errorHandler(err, req, res) {
  res.status(400).send({ message: err.message });
}

const start = async () => {
  try {
    app.listen(PORT);
  } catch (err) {
    errorHandler(err);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);
