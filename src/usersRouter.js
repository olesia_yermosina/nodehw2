const express = require('express');

const router = express.Router();
const { getUser, deleteUser, changeUserPassword } = require('./usersService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/me', authMiddleware, getUser);

router.patch('/me', authMiddleware, changeUserPassword);

router.delete('/me', authMiddleware, deleteUser);

module.exports = {
  usersRouter: router,
};
