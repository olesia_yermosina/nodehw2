const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
  },
}, { timestamps: true });

const Note = mongoose.model('Note', NoteSchema);

module.exports = {
  Note,
};
