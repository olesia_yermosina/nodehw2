const jwt = require('jsonwebtoken');
const { Note } = require('./models/Note');

function createNote(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    const note = new Note({
      userId: tokenPayload.userId,
      text: req.body.text,
    });
    note.save().then(() => res.status(200).json({ message: 'Success' }));
  } catch (err) {
    next(err);
  }
}

function getNotes(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    return Note.find({ userId: tokenPayload.userId }, '-__v').then((result) => {
      res.status(200).json({ notes: result });
    });
  } catch (err) {
    next(err);
  }
}

function getNote(req, res, next) {
  try {
    return Note.findById(req.params.id).then((result) => {
      res.status(200).json({ note: result });
    });
  } catch (err) {
    next(err);
  }
}

async function updateNote(req, res, next) {
  try {
    const note = await Note.findById(req.params.id);
    note.text = req.body.text;
    return note.save().then(() => {
      res.status(200).json({ message: 'Success' });
    });
  } catch (err) {
    next(err);
  }
}

async function changeCompletedNote(req, res, next) {
  try {
    const note = await Note.findById(req.params.id);
    note.completed = !note.completed;
    return note.save().then(() => {
      res.status(200).json({ message: 'Success' });
    });
  } catch (err) {
    next(err);
  }
}

function deleteNote(req, res, next) {
  try {
    return Note.findByIdAndDelete(req.params.id).then(() => {
      res.status(200).json({ message: 'Success' });
    });
  } catch (err) {
    next(err);
  }
}

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  changeCompletedNote,
  deleteNote,
};
