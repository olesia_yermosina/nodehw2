const express = require('express');

const router = express.Router();
const {
  createNote, getNotes, getNote, updateNote, changeCompletedNote, deleteNote,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotes);

router.get('/:id', authMiddleware, getNote);

router.put('/:id', authMiddleware, updateNote);

router.patch('/:id', authMiddleware, changeCompletedNote);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
