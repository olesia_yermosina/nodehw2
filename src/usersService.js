const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/User');

function getUser(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    User.findById(tokenPayload.userId)
      .then((user) => {
        res.status(200).json({ user });
      });
  } catch (err) {
    next(err);
  }
}

function deleteUser(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    User.findByIdAndDelete(tokenPayload.userId)
      .then(() => {
        res.status(200).json({ message: 'Success' });
      });
  } catch (err) {
    next(err);
  }
}

async function changeUserPassword(req, res, next) {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    const user = await User.findById(tokenPayload.userId);
    if (user && await bcrypt.compare(String(req.body.oldPassword), String(user.password))) {
      return User.findByIdAndUpdate(
        { _id: tokenPayload.userId },
        { $set: { password: await bcrypt.hash(req.body.newPassword, 10) } },
      )
        .then(() => {
          res.status(200).json({ message: 'Success' });
        });
    }
    return res.status(400).json({ message: 'Wrong password' });
  } catch (err) {
    next(err);
  }
}

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword,
};
